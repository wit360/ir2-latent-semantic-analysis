from scipy.sparse import lil_matrix, linalg as slinalg
from scipy import sparse, linalg
from numpy import matrix, linalg as nplinalg
import numpy
import math
import sys

from porter_stemmer import PorterStemmer

"""
OVERVIEW
CS4300 Programming Assignment 2
Wit Tatiyanupanwong (NetID wt255)

ACKNOWLEDGEMENT
The program includes Martin Porter's Porter Stemming algorithm
implemented in Python by Vivake Gupta (v@nano.com)
The source code is freely available at from http://tartarus.org/~martin/PorterStemmer/
"""

# we can modify k and number of search result to show here
Ks=[5,10,15,20]
resultPerK=5

# convertQuery
def getQueryConcept(T,S,Dt,wordList,queryTerms):
    queryDoc = lil_matrix( (T.shape[0],1) )

    # create document in word space 
    for i,term in enumerate(wordList):
        if term in queryTerms:
            queryDoc[i,0] = 1 
        
    # create pseudo document in concept space
    # inv(S) x T' x Qd    
    return S.I*T.T*queryDoc


# use index to process user query
def testQuery(wordList,stopList,testDocFiles,query,T,S,Dt):
    # tokenize, stem and filter by stoplist (while checking edge cases)
    tokens = [token.strip().lower() for token in query.split(' ')] 
    tokens = [token for token in tokens if token not in stopList]
    
    # no word remains after stoplist
    if len(tokens) == 0:
        print "Sorry, your query string are all filtered by stop list"
        return
    
    tokens = porterStemmer(tokens)
    # make sure at least on term match
    isMatched = False
    for term in tokens:
        if term in wordList:
            isMatched = True
            break
    if not isMatched:
        print "Sorry, your query string does not match any document"
        return
    
    # try all values of k
    for k in Ks:
        rank = {}
        # convert query into a pseudo document in concept space
        queryConcept = getQueryConcept(T[k],S[k],Dt[k],wordList,tokens)        
        for docID in testDocFiles:
            # cosine similary (document docID)' x queryConcept / norm2(document docID)
            rank[docID] = Dt[k][:,docID].T*queryConcept/linalg.norm(Dt[k][:,docID])
            
        sortedRank = sorted(rank,key=rank.get, reverse=True)[:resultPerK];
        
        print "Search result for k=%d" % ( k )
        for i,docID in enumerate(sortedRank):
            docFile = open(testDocFiles[docID])
            docExcerpts = docFile.read().split(' ')[:15]
            docFile.close()
            print "%2d: %10.7f (%2d) %s" % (i+1,rank[docID],docID,' '.join(docExcerpts))
        print
        
def main():
    # process parameters
    global Ks
    if len(sys.argv) > 2:
        Ks=[int(k) for k in sys.argv[1:]]
    
    # 1. Inverted file setup
    # ----------------------
    testDocFiles = {}
    for docID, docPath in [(id,"test/file%02d.txt" % (id)) for id in range(40)]:
        testDocFiles[docID] = docPath

    stopFile = open("stoplist.txt")
    stopList = stopFile.read().split()
    stopFile.close()
    
    # analyze each document one by one
    wordList = {}
    for docID in testDocFiles:
        analyzeDocument(wordList, stopList, docID, open(testDocFiles[docID]))
    
    # create tf*idf term-document matrix C
    m = len(wordList)     # m terms
    n = len(testDocFiles) # n documents
    C = lil_matrix((m,n)) # C term-document tf.idf matrix
    for i,term in enumerate(wordList.keys()) :
        for j,docID in enumerate(testDocFiles.keys()) :
            tf,idf,tf_idf = calculateStatistic(wordList,term,docID,n);
            if tf_idf != 0:
                C[i,j] = tf_idf;
    
    # 2. Latent Semantic Indxing
    # --------------------------
    # decompose term-document tf*idf matrix into k-dimension concept space
    T,S,Dt = {},{},{}
    for k in Ks:
        T[k],Stemp,Dt[k] = slinalg.svds(C,k=k)
        # expand S to square matrix sorted from highest singular value
        S[k] = matrix(linalg.diagsvd(Stemp, k, k)) 
        T[k] = matrix(T[k])
        Dt[k]= matrix(Dt[k])
        print "T x S x Dt dimensions: ",T[k].shape,S[k].shape,Dt[k].shape

    # 3. Query Processing
    # --------------------
    while True:
        query = raw_input("Please enter query: ")
        if query == "ZZZ":
            break
        elif query.strip() == "":
            continue
        else:
            print("")
            testQuery(wordList,stopList,testDocFiles,query,T,S,Dt)
    print "\nProgram terminated."

# update wordlist and postinglist from a given document id/filepath
def analyzeDocument(wordList,stopList,docID,docFile):
    try:
        docText = docFile.read()
        
        tokens = [token.strip().lower() for token in docText.split(' ')]
        tokens = porterStemmer(tokens)
                
        # for each token
        for position, token in enumerate(tokens):
            # skip words in stoplist
            if token in stopList:
                continue
            
            # first occurence of word
            if token not in wordList:
                wordList[token] = {}     # initialize Word List for the new token
                
            # first occurence of docID
            if docID not in wordList[token]:   # initialize Posting List for the new document
                wordList[token][docID] = []    # - create an empty position list
                
            # save position found
            wordList[token][docID].append(position)
    finally:
        docFile.close()
    return wordList

def porterStemmer(tokens):
    p = PorterStemmer()
    tokens = [p.stem(token, 0,len(token)-1) for token in tokens]
    return tokens

# return (tf, idf, tf.idf)
def calculateStatistic(wordList,term,docID,N):
    # calcualte term frequency
    if term not in wordList or docID not in wordList[term]:
        return (0,0,0)
    
    f_td = len(wordList[term][docID])
    tf   = math.log(f_td, 10)+1 if f_td > 0 else 0
    
    # calculate inverse document frequency
    Nt   = len(wordList[term])
    idf  = math.log(N/float(Nt), 10)
    
    return (tf, idf, tf*idf)


if __name__ == '__main__':
    main()
    
